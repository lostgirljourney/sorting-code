# Sorting Code

### Objective: Write Code to Sort a List

This is a classic problem in computer science for a reason. If you’re new to hacking, keep it simple! If you’re one of our more experienced hackers, try this in a new language or without leveraging search engines.

> Part of LHD: Share Day-3 Daily Challenges
